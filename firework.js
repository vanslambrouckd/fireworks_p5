function Firework(x, y) {
  this.exploded = false;
  this.col = color(random(255),random(255),0);
  this.firework = new Particle(x, y, this.col, true);
  total_nr_particles++;
  this.particles = [];

  this.update = function() {
    if (!this.exploded) {
      this.firework.applyForce(gravity);
      this.firework.update();

      if (this.firework.vel.y >= 0) {
        this.exploded = true;
        this.explode();
        this.firework = null;
      }
    }

    for (var i = this.particles.length-1; i >= 0; i--) {
      //this.particles[i].applyForce(gravity);
      this.particles[i].update();

      if (this.particles[i].done()) {
        this.particles.splice(i, 1);
      }
    }
  }

  this.done = function() {
    if (this.exploded && this.particles.length == 0) {
      total_nr_particles--;
      return true;
    } else {
      return false;
    }
  }

  this.explode = function() {
       for (var i = 0; i < 50; i++) {
         var p = new Particle(this.firework.loc.x, this.firework.loc.y, this.col, false);
         total_nr_particles++;
         this.particles.push(p);
       }
  }

  this.draw = function() {
    if (!this.exploded) {
      this.firework.draw();
    }

    for (var i = 0; i < this.particles.length; i++) {
      this.particles[i].draw();
    }
  }
}
