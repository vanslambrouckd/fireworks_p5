//p5.disableFriendlyErrors = true;

var fireworks = [];
var gravity;
var total_nr_particles = 0;
var pause = false;
var fps;

function draw() {
  background(0);

  fps = frameRate();
  fill(100);
  stroke(0);
  text("FPS: " + fps.toFixed(2), 10, height - 10);


  if (random(1) < 0.2 && !pause) {
    particle = new Firework(random(width), height-5);
    fireworks.push(particle);
  }

  // for (i = 0; i < fireworks.length; i++) {
  for (i = fireworks.length-1; i >= 0; i--) {
    fireworks[i].update();
    fireworks[i].draw();

    if (fireworks[i].done()) {
      fireworks.splice(i, 1);
    }
  }
  // console.log('#fireworks:'+total_nr_particles);
}

function setup() {
  // frameRate(2);
  createCanvas(640,300);
  gravity = createVector(0, 0.2);

  particle = new Firework(random(width), height-5);
  fireworks.push(particle);
}

function mousePressed() {
  particle = new Firework(mouseX, height-5);
  fireworks.push(particle);
}
