function Particle(x, y, col, firework) {
  this.col = col;
  this.firework = firework;
  this.lifespan = 255;
  //vel = velocity = rate of change of object in meter/sec
  if (this.firework) {
    // this.vel = createVector(0,-14);
    this.vel = createVector(0,random(-12, -8));
  } else {
    //this.vel = createVector(random(-10, 10), random(-10,10));
    this.vel = p5.Vector.random2D(); //unit vector, dus alles explode in circle
    this.vel.mult(random(2, 4));
  }
  //accel = accelerator = rate of change of velocity in meter/second()
  this.accel = createVector(0,0);
  this.loc = createVector(x, y);


  this.applyForce = function(force) {
    this.accel.add(force);
  }

  this.update = function() {
    this.vel.add(this.accel);
    this.loc.add(this.vel);
    this.accel.mult(0); //accelerator resettten
    if (!this.firework) {
      this.lifespan -= 5;
    }
    // print('velocity='+this.vel.y);
  }

  this.done = function() {
    if (this.lifespan < 0) {
      total_nr_particles--;
      return true;
    } else {
      return false;
    }
  }

  this.draw = function() {
    strokeWeight(4);
    stroke(red(this.col), green(this.col), blue(this.col), this.lifespan);
//    stroke(random(0,255), random(0,255), random(0,255), this.lifespan);
    point(this.loc.x, this.loc.y);
  }
}
